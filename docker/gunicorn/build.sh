#!/usr/bin/env bash
set -x -e

buildnumber=${4-$(date -u +"%y%m%d%H%M")}

docker build --no-cache -t "$1"/resumenlp:3.7_"$buildnumber" .
docker tag "$1"/resumenlp:3.7_"$buildnumber" "$1"/resumenlp:latest
