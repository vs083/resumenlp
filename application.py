from flask import Flask, request, jsonify, abort
from flask_restful import Resource, Api
from sqlalchemy import create_engine
# import connexion
import json
import os
import time
import nltk
from json import dumps, loads, JSONEncoder, JSONDecoder
import pickle
from fuzzywuzzy import fuzz
from nltk.stem import WordNetLemmatizer
nltk.data.path.append('/home/site/wwwroot/resumenlp/')
nltk.download('wordnet', download_dir='/home/site/wwwroot/resumenlp/')
lemmatizer = WordNetLemmatizer()

app = Flask(__name__)


# app.config["DEBUG"] = True
# api = Api(app)

# Create the application instance
# app = connexion.App(__name__, specification_dir='./')

# Read the swagger.yml file to configure the endpoints
# app.add_api('swagger.yml')


@app.route('/', methods=['GET'])
def home():
    return "<h1>Resume Service NLP</h1><p>This site is an API for NLP.</p>"


# A route to return all the matching job titles.
@app.route('/api/v1/jobtitles/match', methods=['GET'])
def get_matching_job_titles():
    if 'job_title' in request.args:
        job_title = request.args['job_title']
    else:
        return "Error: No id field provided. Please specify an id."
    obj = NearestTitlePredictor()
    results = obj.get_nearest_title(job_title)
    # return jsonify(results)
    return json.dumps(results, default=set_default)


# A route to check whether the target skills and resume skills match
@app.route('/api/v1/skills/check', methods=['POST'])
def check_resume_skill_with_target_skill():
    # if 'clean_skill_list' in request.args:
    #     clean_skill_list = request.args.getlist('clean_skill_list')
    # else:
    #     return "Error: No clean_skill_list field provided. Please specify clean_skill_list."

    # if 'target_skills' in request.args:
    #     target_skills = request.args.getlist('target_skills')
    # else:
    #     return "Error: No target_skills field provided. Please specify target_skills."
    if not request.json or not 'clean_skill_list' or not 'target_skills' in request.json:
        abort(400)
    clean_skill_list = request.json['clean_skill_list']
    target_skills = request.json['target_skills']
    objSkills = ParseResume()
    res = objSkills.resume_skill_target_skill_intersection(clean_skill_list, target_skills)

    return str(res)


@app.errorhandler(404)
def page_not_found(e):
    return "<h1>404</h1><p>The resource could not be found.</p>", 404


def set_default(obj):
    if isinstance(obj, set):
        return list(obj)
    raise TypeError


class Singleton(type):
    '''Singleton Meta class'''
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class NearestTitlePredictor(metaclass=Singleton):

    def __init__(self):
        self.titles = set()
        self.dir_path = os.path.dirname(os.path.realpath(__file__))

        with open(os.path.join(self.dir_path, "job-titles.txt"), "r") as fp:
            x = fp.read()
            y = json.loads(x)
            for data in y:
                self.titles.add(data)

        self.inverted_title_index = {}
        for title in self.titles:
            token = title.lower().split()
            for x in token:
                lemma = lemmatizer.lemmatize(x)
                prev = self.inverted_title_index.get(lemma, set())
                prev.add(title.lower())
                self.inverted_title_index[lemma] = prev

    def get_nearest_title(self, target_title=""):
        result_set = set()
        result_title = {}

        title_tokens = target_title.lower().split()
        probable_titles = set()
        for val in title_tokens:
            # print(val)
            # print(self.inverted_title_index)
            titles = self.inverted_title_index.get(val)
            if titles:
                probable_titles.update(titles)

        for data in probable_titles:
            result = fuzz.token_sort_ratio(target_title, data)
            result_title[data] = result

        result_title = sorted(result_title.items(), key=lambda kv: (kv[1], kv[0]), reverse=True)

        count = 0
        for val in result_title:
            # print(val)
            result_set.add(val[0])
            count += 1
            if count == 2:
                break

        return result_set
        # return json.dumps(result_set, default=set_default)


# api.add_resource(NearestTitlePredictor, '/nearest_title/<target_title>')  # Route_1

class ParseResume(metaclass=Singleton):

    def resume_skill_target_skill_intersection(self, clean_skill_list=[], target_skills=[]):
        '''
        We do below major actions here:
        for every target skill:
            for every resume skill:
                1) check if raw target skill text and raw resume skill text have >= 80 token sort ratio
                2) check if raw target skill is present in raw resume skill
                3) check if lemma of raw target skill if present in lemma of raw resume skill (resume skill is divided in multiple tokens so we have list of lemma)
                4) check if clean target skills text is present in clean resume skill text (clean means only noun and verbs in consideration)
                5) check if clean target skill text and clean resume skill text have >= 80 token sort ratio
                6) check if lemma of clean target skill text is present in array of lemma of clean resume skill text
            if any of above 6 conditions satisfies mark skill found flag and count
            stop iterating if 3 skills are found matching the criterian
        '''

        # return false if less than 3 skills present in resume
        # print(len(clean_skill_list))
        if len(clean_skill_list) < 3:
            return False

        # print(len(target_skills))
        # return false if less than 3 skills present in target skills
        if len(target_skills) < 3:
            return True

        # ignore words of these tags from resume skills only accept nouns and verbs (its used in corner case only)
        ignore_tag = ["CC", "CD", "DT", "EX", "FW", "IN", "JJ", "JJR", "JJS", "LS", "MD", "PDT", "PRP$", "PRP", "RB",
                      "RBR",
                      "RBS", "RP", "TO", "UH", "WDT", "WP", "WP$", "WRB"]
        # ignore below words if the skills contains more words in addition of these
        # in case these words are as it is present in resume skills then keep it
        ignore_word = ["skills", "skill", "operations", "operation"]
        skill_found = 0

        # iterate through target skills
        for skill in target_skills:
            # stop computing if you found 3 matching skills
            if skill_found == 3:
                break

            max_fuzz = 0
            text_found = False
            lemma_found = False

            # ignore common words from skills
            v = skill
            for x in ignore_word:
                v = v.replace(x, "")
            v = v.strip()

            # get all tokens from cleaned skill text
            temp_skill = v.lower().split()
            # pos tag all tokens
            ts_pos = nltk.pos_tag(temp_skill)
            text_temp = ""
            if len(temp_skill) == 0:
                # keep original text of skill if after removing frequent words it becomes blank
                # this is for scenarios where operation is passed as a skill
                text_temp = skill
            else:
                # re-create a text from cleaned skills
                # only consider words which are not in ignore word and ignore_tag
                if len(temp_skill) == 1:
                    text_temp = temp_skill[0]
                else:
                    for x in ts_pos:
                        if len(ts_pos) == 1:
                            # just to avoid a space before the text, could have used a strip also
                            text_temp = skill
                        else:
                            if x[1] not in ignore_tag and x[0] not in ignore_word:
                                text_temp = text_temp + " " + x[0]
                    text_temp = text_temp.strip()

            # lemmatize the target skill
            lemm_job_skill = lemmatizer.lemmatize(skill, pos="v")

            # iterate through the resume skills
            for sk in clean_skill_list:
                # check distance of raw resume skills and raw target skills
                # 1) check if raw target skill text and raw resume skill text have >= 80 token sort ratio
                f_score = fuzz.token_sort_ratio(skill.lower(), sk.lower())
                if f_score > max_fuzz:
                    max_fuzz = f_score
                    # if token sort ratio is greater than or equal to 80 mark it found
                    if max_fuzz >= 80:
                        break

                # if target skill is present as it is in resume skill then mark it found
                # 2) check if raw target skill is present in raw resume skill
                if skill.lower() in sk.lower():
                    text_found = True
                    break

                # lemmstize resume skill after toknizing it
                # pos="v" helps constrain verb part of speech
                # going, gone, goes, went all becomes go
                # its done mainly for scenarios where people write skills in different tense
                # example : teaching, teach, teaches, taught all will become teach
                lemm_resume_skill = sk.split()
                lemm_resume_skill = [lemmatizer.lemmatize(x, pos="v") for x in lemm_resume_skill]

                # if target skill lemma present in lemmas of resume skill list mark it found
                # 3) check if lemma of raw target skill if present in lemma of raw resume skill
                # (resume skill is divided in multiple tokens so we have list of lemma)
                if lemm_job_skill in lemm_resume_skill:
                    lemma_found = True
                    break

                if len(text_temp) > 0:
                    v_sk_text = ""
                    v = sk
                    # ignore common words from resume skills
                    for x in ignore_word:
                        v = v.replace(x, "")
                    v = v.strip()

                    # temp_skill = v.lower().split()
                    t = v.lower().split()
                    # pos tag tokenized resume skill text
                    t_pos = nltk.pos_tag(t)

                    # if after ignoring common word resume skill becomes blank then donot ignore common words
                    if len(t) == 0:
                        v_sk_text = sk.lower()
                    else:
                        if len(t) == 1:
                            v_sk_text = t[0]
                        else:
                            for x in t_pos:
                                if x[1] not in ignore_tag and x[0] not in ignore_word:
                                    v_sk_text = v_sk_text + " " + x[0]
                            # compose a clean resume skill text composing of only noun and verbs
                            v_sk_text = v_sk_text.strip()

                    if len(v_sk_text) == 0:
                        continue

                    # check if cleaned target skills is in cleaned resume skill
                    # 4) check if clean target skills text is present in clean resume skill text
                    # (clean means only noun and verbs in consideration)
                    if text_temp.lower() in v_sk_text.lower():
                        text_found = True
                        break

                    # check distance of cleaned target skills with cleaned resume skill
                    # 5) check if clean target skill text and clean resume skill text have >= 80 token sort ratio
                    if fuzz.token_sort_ratio(text_temp.lower(), v_sk_text.lower()) >= 80:
                        max_fuzz = fuzz.token_sort_ratio(text_temp.lower(), v_sk_text.lower())
                        break

                    # lemmatize clean target skill
                    lemm_job_skill = lemmatizer.lemmatize(text_temp, pos="v")
                    # lemmatize clean resume skill
                    lemm_resume_skill = v_sk_text.split()
                    lemm_resume_skill = [lemmatizer.lemmatize(x, pos="v") for x in lemm_resume_skill]
                    # check if lemma of clean target skill is present in lemma of clean resume skill
                    # 6) check if lemma of clean target skill text is present in array of lemma of clean
                    # resume skill text
                    if lemm_job_skill in lemm_resume_skill:
                        lemma_found = True
                        break

            if max_fuzz >= 80 or text_found == True or lemma_found == True:
                skill_found += 1

        if skill_found >= 3:
            return True
        else:
            return False


if __name__ == '__main__':
    app.run(host='0.0.0.0', port='8000')